\contentsline {section}{\numberline {1}Uvod}{3}{section.1}%
\contentsline {section}{\numberline {2}Opis problema}{4}{section.2}%
\contentsline {section}{\numberline {3}Algoritmi i metode}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}ARIMA model (AutoRegressive Integrated Moving Average) }{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}SARIMA - Seasonal Autoregressive Integrated Moving Average }{5}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Rekurentne neuronske mreže- RNN }{6}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}LSTM Mreže - Long short-term memory}{6}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Gated Recurrent Unit (GRU) mreže}{7}{subsubsection.3.3.2}%
\contentsline {subsection}{\numberline {3.4}LOWESS - Local Regression Smoothing}{7}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}STL metodologija}{8}{subsection.3.5}%
\contentsline {section}{\numberline {4}Analiza vremenskih serija kretanja kursa dinara u odnosu na evro u periodu od 2003. do 2023. godine}{9}{section.4}%
\contentsline {subsection}{\numberline {4.1}Primena STL dekompozicije nad podacima za kretanje kursa dinara u odnosu na evro}{13}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}ARIMA model treniran nad podacima za kretanje kursa dinara u odnosu na evro}{14}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}ARIMA sa podešenim hiperparametrima nad podacima za kretanje kursa dinara u odnosu na vero}{15}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}ARIMA u RapidMiner-u}{17}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}SARIMA model treniran nad podacima za kretanje kursa dinara u odnosu na evro}{17}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}GRU mreža nad podacima za kretanje kursa dinara u odnosu na evro}{18}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}LSTM mreža nad podacima za kretanje kursa dinara u odnosu na evro}{19}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}LOWEES nad podacima za kretanje kursa dinara u odnosu na evro}{19}{subsection.4.8}%
\contentsline {section}{\numberline {5}Analiza vremenskih serija prosečnih temperatura u Begradu u periodu od 1887. do 2017. godine}{21}{section.5}%
\contentsline {subsection}{\numberline {5.1}Sređivanje podataka i vizuaizacija}{21}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Primena ARIMA algoritma na podatke o temperaturama}{24}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Primena SARIMA algoritma na podatke o temperaturama}{26}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Primena LOWESS algoritma na podatke o temperaturama}{27}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Primena LSTM algoritma na podatke o temperaturama}{29}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}Primena STL dekompozicije na podatke o temperaturama }{29}{subsection.5.6}%
\contentsline {subsection}{\numberline {5.7}Primena GRU mreže na podatke o temperaturama}{31}{subsection.5.7}%
\contentsline {section}{\numberline {6}Zaključak}{32}{section.6}%
\contentsline {subsection}{\numberline {6.1}Kretanje kursa u odnosu na evro}{32}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Prosečne temperature u Beogradu}{32}{subsection.6.2}%
\contentsline {section}{\numberline {7}Literatura}{34}{section.7}%
